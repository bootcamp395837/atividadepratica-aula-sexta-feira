﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="atividadepraticaaulasextafeira.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Exemplo do Panel em ASP.NET
        </div>
        <asp:Table ID="Table1" runat="server"></asp:Table>
        <asp:Panel ID="Panel1" runat="server" Height="44px"></asp:Panel> <br />
        <asp:Label ID="Label1" runat="server" Text="Informações Pessoais"></asp:Label><br /><br />
        <asp:Label ID="Label3" runat="server" Text="Nome:"></asp:Label><br />
        <asp:TextBox ID="TextBox1" runat="server" BorderStyle="Solid" Width="350px"></asp:TextBox>  <br />
        <asp:Label ID="Label4" runat="server" Text="Sobrenome:"></asp:Label><br />
        <asp:TextBox ID="TextBox2" runat="server" BorderStyle="Solid" Width="350px"></asp:TextBox> <br />
        <asp:Label ID="Label5" runat="server" Text="Gênero:"></asp:Label><br />
        <asp:TextBox ID="TextBox3" runat="server" BorderStyle="Solid"></asp:TextBox> <br />
        <asp:Label ID="Label6" runat="server" Text="Celular:"></asp:Label><br />
        <asp:TextBox ID="TextBox4" runat="server" TextMode="Number" BorderStyle="Solid"></asp:TextBox> <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Próximo" BorderStyle="Dotted" /> <br /><br />
        <asp:Label ID="Label2" runat="server" Text="Detalhes do Endereço"></asp:Label> <br />

        <asp:Panel ID="Panel2" runat="server" Visible="False"></asp:Panel> <br />
        <asp:Label ID="Label9" runat="server" Text="Endereço:"></asp:Label><br />
        <asp:TextBox ID="TextBox5" runat="server" BorderStyle="Solid" Width="450px"></asp:TextBox><br />
        <asp:Label ID="Label7" runat="server" Text="Cidade:"></asp:Label><br />
        <asp:TextBox ID="TextBox6" runat="server" BorderStyle="Solid" Width="450px"></asp:TextBox><br />
        <asp:Label ID="Label8" runat="server" Text="CEP:"></asp:Label><br />
        <asp:TextBox ID="TextBox7" runat="server" TextMode="Number" BorderStyle="Solid"></asp:TextBox><br /><br />
        <asp:Button ID="Button2" runat="server" Text="Voltar" BorderStyle="Dotted" />
        <asp:Button ID="Button3" runat="server" Text="Próximo" BorderStyle="Dotted" />
        <asp:Panel ID="Panel3" runat="server" Visible="False"></asp:Panel> <br />
        <asp:Label ID="Label10" runat="server" Text="Area de Login"></asp:Label><br /><br />
        <asp:Label ID="Label11" runat="server" Text="Usuário:"></asp:Label><br />
        <asp:TextBox ID="TextBox8" runat="server" BorderStyle="Solid"></asp:TextBox><br />   
        <asp:Label ID="Label12" runat="server" Text="Senha:"></asp:Label><br />
        <asp:TextBox ID="TextBox9" runat="server" BorderStyle="Solid" TextMode="Password"></asp:TextBox><br />
        <asp:Button ID="Button4" runat="server" Text="Voltar" BorderStyle="Dotted" />
        <asp:Button ID="Button5" runat="server" Text="Enviar" BorderStyle="Dotted" />
        <asp:Panel ID="Panel4" runat="server"></asp:Panel> <br />

    </form>
</body>
</html>
